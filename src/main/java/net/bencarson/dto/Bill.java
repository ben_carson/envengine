package net.bencarson.dto;

import java.util.Date;

public class Bill {

	private String name;
	private double amount;
	private Date arriveDate;
	private Date dueDate;
	private boolean isPaid;
	private String note;
	private Date autoPayDate;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public double getAmount() {
		return amount;
	}
	public void setAmount(double amount) {
		this.amount = amount;
	}
	public Date getArriveDate() {
		return arriveDate;
	}
	public void setArriveDate(Date arriveDate) {
		this.arriveDate = arriveDate;
	}
	public Date getDueDate() {
		return dueDate;
	}
	public void setDueDate(Date dueDate) {
		this.dueDate = dueDate;
	}
	public boolean isPaid() {
		return isPaid;
	}
	public void setPaid(boolean isPaid) {
		this.isPaid = isPaid;
	}
	public String getNote() {
		return note;
	}
	public void setNote(String note) {
		this.note = note;
	}
	public Date getAutoPayDate() {
		return autoPayDate;
	}
	public void setAutoPayDate(Date autoPayDate) {
		this.autoPayDate = autoPayDate;
	}
	
}
