package net.bencarson.envelope.engine.test;

import static org.junit.Assert.*;

import net.bencarson.envelope.engine.EnvEngine;

import org.junit.Test;

public class EngineTest {

	@Test
	public void testComputeTwenties() {
		double testValue = 30.24;
		int expected = (int) (testValue / 20);
		int answer = EnvEngine.computeTwenties(testValue);
		assertTrue("Expected " + expected + " Jacksons, got "+answer,answer == expected);
	}

	@Test
	public void testComputeTens() {
		double testValue = 26.39;
		int expected = (int) (testValue / 10);
		int answer = EnvEngine.computeTens(testValue);
		assertTrue("Expected " + expected + "tens, got "+answer, answer==expected);
	}

	@Test
	public void testComputeFivers() {
		double testValue = 16.05;
		int expected = (int) (testValue / 5);
		int answer = EnvEngine.computeFivers(testValue);
		assertTrue("Expected " + expected + " fives, got "+answer, answer == expected);
	}

	@Test
	public void testComputeSingles() {
		double testValue = 4.33;
		int expected = (int) (testValue / 1);
		int answer = EnvEngine.computeSingles(testValue);
		assertTrue("Expected " + expected + " singles, got "+answer, answer == expected);
	}

	@Test
	public void testComputeQuarters() {
		double testValue = 3.00;
		int expected = (int) (testValue / .25);
		int answer = EnvEngine.computeQuarters(testValue);
		assertTrue("Expected "+ expected +" quarters, got "+answer, answer == expected);
	}

	@Test
	public void testComputeDimes() {
		double testValue = 2.16;
		int expected = (int) (testValue / .10);
		int answer = EnvEngine.computeDimes(testValue);
		assertTrue("Expected " + expected + " dimes, got "+answer, answer == expected);
	}

	@Test
	public void testComputeNickels() {
		double testValue = .25;
		int expected = (int) (testValue / .05);
		int answer = EnvEngine.computeNickels(testValue);
		assertTrue("Expected " + expected + " nickels, got "+answer, answer == expected);
	}

	@Test
	public void testComputePennies() {
		double testValue = .17;
		int expected = (int) (testValue/.01);
		int answer = EnvEngine.computePennies(testValue);
		assertTrue("Expected "+expected+" pennies, got "+ answer,answer == expected);
	}

}
